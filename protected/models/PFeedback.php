<?php

  /**
   * Created by PhpStorm.
   * User: cranky4
   * Date: 24.03.15
   * Time: 12:09
   */

  Yii::import('ygin.modules.feedback.models.Feedback');

  class PFeedback extends Feedback
  {

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
      return array(
        array(
          'fio, phone',
          'required'
        ),
        array(
          'mail',
          'email',
          'message' => 'Введен некорректный e-mail адрес'
        ),
        array(
          'phone',
          'match',
          'pattern' => '/^\d{11}$|^\d{6}$/s',
          'message' => 'Введите 10 значный номер телефона'
        ),
        array(
          'fio',
          'match',
          'pattern' => '/^[A-Za-zЙФЯЦЫЧУВСКАМЕПИНРТОЬШЛБЩДЮЗЖХЪйфяцычувскамепинртгоьшлбщдюзжхэъ\s-]+$/i',
          'message' => 'Введите корректные данные'
        ),
        array(
          'fio',
          'length',
          'max' => 100
        ),
//        array(
//          'verifyCode',
//          'DaCaptchaValidator',
//          'caseSensitive' => true
//        ),
        array(
          'fio, phone, mail',
          'length',
          'max' => 255
        ),
      );
    }
  }