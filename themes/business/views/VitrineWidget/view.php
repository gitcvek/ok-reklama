<?php $this->registerCssFile('vitrine.css');?>

<div id="ygin-vitrine-carousel" class="b-vitrine carousel slide" data-ride="carousel">
				 <?php /* <!-- Indicators -->
				  <ol class="carousel-indicators">
				<?php 
				$i = 0;
				foreach ($models AS $model) {
				  $activeClass = ($i == 0) ? ' class="active"' : '';
				
				?>
				  <li data-target="#ygin-vitrine-carousel" data-slide-to="<?php echo $i ?>"<?php echo $activeClass ?>></li>
				<?php
				  $i++;
				}
				?>
				  </ol>
					*/?>
	<?php /*	  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
		<?php
		$i = 0;
		foreach ($models AS $model) {
		  $id    = $model->id_vitrine;
		  $class = ($i == 0) ? 'item active' : 'item';
		  $link  = $model->link;
		  $imgHTML = '';
		  if ($model->file) {
		    $imgHTML = '<img src="'.$model->file->getUrlPath().'" alt="'.$model->title.'">';
		  } else {
		    $imgHTML = '<div class="img"></div>';
		  }
		?>
		    <div class="<? echo $class ?>">
		      <?php echo $imgHTML ?>
		      <div class="carousel-caption">
		        <h3 class="title"><a href="<?php echo $link; ?>"><?php echo $model->title; ?></a></h3>
		        <div class="text"><?php echo $model->text; ?></div>
		      </div>
		    </div>
		<?php
		  $i++;
		}
		?>
		  </div>
  */?>
  
<?php 
  $skitterPath = Yii::getPathOfAlias('webroot.themes.business.js.skitter.images').DIRECTORY_SEPARATOR;

  Yii::app()->clientScript->registerCssFile('/themes/business/js/skitter/css/skitter.styles.css');
  Yii::app()->clientScript->addDependResource('skitter.styles.css', array(
    $skitterPath.'back-box-label-black.png' => '../images/',
    $skitterPath.'ajax-loader.gif' => '../images/',
    $skitterPath.'background.gif' => '../images/',
    $skitterPath.'focus-button.png' => '../images/',
    $skitterPath.'next.png' => '../images/',
    $skitterPath.'pause-button.png' => '../images/',
    $skitterPath.'play-button.png' => '../images/',
    $skitterPath.'prev.png' => '../images/',
  ));

Yii::app()->clientScript->registerScriptFile('/themes/business/js/skitter/js/jquery.easing.1.3.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile('/themes/business/js/skitter/js/jquery.animate-colors-min.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile('/themes/business/js/skitter/js/jquery.skitter.min.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript('skitter.init', '
  $(".box_skitter").skitter({
    animation: "randomSmart",
    numbers: true,
    navigation : false,
    controls_position: "rightTop",
    controls: false,
    hideTools: true,
    interval: 5000,
    show_randomly: true,
  });', CClientScript::POS_READY);
?>
<div class="header-doska-unit_large box_skitter">
<ul> 
		<?php foreach ($models AS $model) :?>
		<li>
			<!--  <a href="<?=$model->link;?>">  -->
				<img title="<?=$model->title;?>" alt="<?=$model->title;?>" 
				src="<?=$model->file->getUrlPath()?>" 
				height="500" width="1170">				
			<!--  </a>-->
		</li>
		<?php endforeach;?>
</ul>
</div>


  
  
  
  
  
						
						  <!-- Controls 
						  <a class="left carousel-control" href="#ygin-vitrine-carousel" data-slide="prev">
						    <span class="glyphicon glyphicon-chevron-left"></span>
						  </a>
						  <a class="right carousel-control" href="#ygin-vitrine-carousel" data-slide="next">
						    <span class="glyphicon glyphicon-chevron-right"></span>
						  </a>
						</div> -->
	