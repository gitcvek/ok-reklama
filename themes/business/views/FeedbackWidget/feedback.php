<button class="call-me" data-toggle="modal" data-target=".b-callback-modal">Обратный звонок</button>
<div class="modal fade b-callback-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
  <?php
    if (Yii::app()->user->hasFlash('feedback-success')) {
      $this->widget('AlertWidget', array(
        'title' => 'Обратная связь',
        'message' => Yii::app()->user->getFlash('feedback-success'),
      ));
    }

    $this->registerCssFile('b-feedback-widget.css');

    $hidden = 'none';
    if (Yii::app()->user->hasFlash("feedback-message")) {
      $hidden = 'block';
      //echo '<i>'.Yii::app()->user->getFlash('feedback-message').'</i>';
    }

    /**
     * @var $form CActiveForm
     * @var $model PFeedback
     */
    $form = $this->beginWidget('CActiveForm', array(
      'id' => 'feedbackForm',
      'enableAjaxValidation' => true,
      'enableClientValidation' => true,
      'focus' => array(
        $model,
        'fio'
      ),
      'htmlOptions' => array(
        //'class' => 'form-horizontal',
        //'style' => 'display:'.$hidden.';',
      ),
      'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false,
      ),
      'errorMessageCssClass' => 'label label-danger',
      'action' => Yii::app()->createUrl(FeedbackModule::ROUTE_FEEDBACK),
    ));

  ?>
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Обратный звонок</h4>
      </div>
      <div class="modal-body">
        <? //php echo $form->errorSummary($model);?>

        <div class="form-group">
          <?php echo $form->labelEx($model, 'phone', array("class" => "")); ?>
          <?php echo $form->textField($model, 'phone', array(
            'class' => 'form-control',
            'placeholder' => '79121122333'
          )); ?>
          <?php echo $form->error($model, 'phone', array('class' => 'label label-danger')); ?>
        </div>

        <div class="form-group">
          <?php echo $form->labelEx($model, 'fio', array("class" => "")); ?>
          <?php echo $form->textField($model, 'fio', array(
            'class' => 'form-control',
            'placeholder' => 'Иван Иванович'
          )); ?>
          <?php echo $form->error($model, 'fio', array('class' => 'label label-danger')); ?>
        </div>

        <button type="submit" class="btn btn-warning"><i class="mdi-communication-call"></i> Позвоните мне
        </button>
      </div>
    </div>
  </div>
  <?php $this->endWidget(); ?>
</div>



