<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <meta name="viewport" content="width=1170">
  <meta http-equiv="content-language" content="ru"> <?php // TODO - в будущем генетить автоматом ?>



  <?php
    //Регистрируем файлы скриптов в <head>
    if (YII_DEBUG) {
      Yii::app()->assetManager->publish(YII_PATH . '/web/js/source', false, -1, true);
    }

    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerCoreScript('bootstrap');
    $bootstrapFont = Yii::getPathOfAlias('application.assets.bootstrap.fonts') . DIRECTORY_SEPARATOR;
    Yii::app()->clientScript->addDependResource('bootstrap.min.css', array(
      $bootstrapFont . 'glyphicons-halflings-regular.eot' => '../fonts/',
      $bootstrapFont . 'glyphicons-halflings-regular.svg' => '../fonts/',
      $bootstrapFont . 'glyphicons-halflings-regular.ttf' => '../fonts/',
      $bootstrapFont . 'glyphicons-halflings-regular.woff' => '../fonts/',
    ));

    Yii::app()->clientScript->registerScriptFile('/themes/business/js/js.js', CClientScript::POS_HEAD);

    Yii::app()->clientScript->registerScript('setScroll', "setAnchor(); $.material.init();", CClientScript::POS_READY);
    Yii::app()->clientScript->registerScript('menu.init', "$('.dropdown-toggle').dropdown();", CClientScript::POS_READY);

    Yii::app()->clientScript->registerCssFile('/themes/business/css/content.css');
    Yii::app()->clientScript->registerCssFile('/themes/business/css/page.css');

    // Yii::app()->clientScript->registerCssFile('/themes/business/dist/css/roboto.min.css');
    Yii::app()->clientScript->registerCssFile('/themes/business/dist/css/material.min.css');
    Yii::app()->clientScript->registerCssFile('/themes/business/dist/css/ripples.min.css');

    Yii::app()->clientScript->registerScriptFile('/themes/business/dist/js/ripples.min.js');
    Yii::app()->clientScript->registerScriptFile('/themes/business/dist/js/material.min.js');

  ?>
  <title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
</head>
<body>
<div class="wrap-list">
  <div id="wrap" class="container">
    <div id="head" class="row">
      <?php if (Yii::app()->request->url == "/") { ?>
        <div class="logo col-xs-3">
          <img border="0" alt="Название компании - На главную" src="/themes/business/gfx/logo.png"></div>
      <?php } else { ?>
        <a href="/" title="Главная страница" class="logo col-md-2"><img src="/themes/business/gfx/logo.png" alt="Логотип компании"></a>
      <?php } ?>
      <div class="cname col-xs-6">
        <ul>
          <li>
            <a href="#modal-about" data-toggle="modal">
              <div class="item">
                <i class="fa fa-flag"></i>

                <p>О компании</p>
              </div>
            </a>
          </li>
          <li>
            <a href="#modal-klient" data-toggle="modal">
              <div class="item">
                <i class="fa fa-users"></i>

                <p>Наши клиенты</p>
              </div>
            </a>
          </li>
          <li>
            <a href="#modal-reviews" data-toggle="modal">
              <div class="item">
                <i class="fa fa-comment"></i>

                <p>Отзывы</p>
              </div>
            </a>
          </li>
          <li>
            <a href="#modal-kontakt" data-toggle="modal">
              <div class="item">
                <i class="fa fa-envelope"></i>

                <p>Контакты</p>
              </div>
            </a>
          </li>
        </ul>
      </div>
      <div class="tright col-xs-3">
        <div class="numbers">
          <p>+7(8212)<strong>34-62-72</strong></p>
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_RIGHT)); ?>
        </div>
      </div>
    </div>
    <?php /* <div class="b-menu-top navbar navbar-default" role="navigation">
      <div class="collapse navbar-collapse">
<?php

if (Yii::app()->hasModule('search')) {
  $this->widget('SearchWidget');
}
$this->widget('MenuWidget', array(
  'rootItem' => Yii::app()->menu->all,
  'htmlOptions' => array('class' => 'nav navbar-nav'), // корневой ul
  'submenuHtmlOptions' => array('class' => 'dropdown-menu'), // все ul кроме корневого
  'activeCssClass' => 'active', // активный li
  'activateParents' => 'true', // добавлять активность не только для конечного раздела, но и для всех родителей
  //'labelTemplate' => '{label}', // шаблон для подписи
  'labelDropDownTemplate' => '{label} <b class="caret"></b>', // шаблон для подписи разделов, которых есть потомки
  //'linkOptions' => array(), // атрибуты для ссылок
  'linkDropDownOptions' => array('data-target' => '#', 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'), // атрибуты для ссылок для разделов, у которых есть потомки
  'linkDropDownOptionsSecondLevel' => array('data-target' => '#', 'data-toggle' => 'dropdown'), // атрибуты для ссылок для разделов, у которых есть потомки
  //'itemOptions' => array(), // атрибуты для li
  'itemDropDownOptions' => array('class' => 'dropdown'),  // атрибуты для li разделов, у которых есть потомки
  'itemDropDownOptionsSecondLevel' => array('class' => 'dropdown-submenu'),
//  'itemDropDownOptionsThirdLevel' => array('class' => ''),
  'maxChildLevel' => 2,
  'encodeLabel' => false,
));

?>
      </div>
    </div>
*/ ?>

    <?php /*
  $skitterPath = Yii::getPathOfAlias('webroot.themes.business.js.skitter.images').DIRECTORY_SEPARATOR;

  Yii::app()->clientScript->registerCssFile('/themes/business/js/skitter/css/skitter.styles.css');
  Yii::app()->clientScript->addDependResource('skitter.styles.css', array(
    $skitterPath.'back-box-label-black.png' => '../images/',
    $skitterPath.'ajax-loader.gif' => '../images/',
    $skitterPath.'background.gif' => '../images/',
    $skitterPath.'focus-button.png' => '../images/',
    $skitterPath.'next.png' => '../images/',
    $skitterPath.'pause-button.png' => '../images/',
    $skitterPath.'play-button.png' => '../images/',
    $skitterPath.'prev.png' => '../images/',
  ));

Yii::app()->clientScript->registerScriptFile('/themes/business/js/skitter/js/jquery.easing.1.3.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile('/themes/business/js/skitter/js/jquery.animate-colors-min.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile('/themes/business/js/skitter/js/jquery.skitter.min.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript('skitter.init', '
  $(".box_skitter").skitter({
    animation: "randomSmart",
    numbers: true,
    navigation : false,
    controls_position: "rightTop",
    controls: false,
    hideTools: true,
    interval: 5000,
    show_randomly: true,
  });', CClientScript::POS_READY);
?>
<div class="header-doska-unit_large box_skitter">
<ul>
<li><a href="/price/21/"><img title="Интернет-магазин" alt="Интернет-магазин" src="/content/modules/120/internet-magazin.jpg" height="407" width="960"></a></li>
<li><a href="/price/6/"><img title="Корпоративный сайт" alt="Корпоративный сайт" src="/content/modules/120/corporative.jpg" height="407" width="960"></a></li>
</ul>
</div>
*/ ?>

    <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_TOP)); ?>


    <div class="b-menu-bottom btn-group btn-group-justified">
      <a href="#modal" class="btn btn-default">

        <i class="fa fa-map-marker"></i> Адресная программа

      </a> <a href="#modal-razm" class="btn btn-default">

        <i class="fa fa-cog"></i> Размещение

      </a> <a href="#modal-print" class="btn btn-default">

        <i class="fa fa-delicious"></i>

        <div class="shir">
          Широкоформатная печать
        </div>
      </a>
    </div>
    <!--
    <div class="b-menu-bottom">
            <a href="#modal" data-toggle="modal">
                    <div class="adres">
                            <i class="fa fa-map-marker"></i>
                                    Адресная программа
                    </div>
            </a>
            <a href="#modal-razm" data-toggle="modal">
                    <div class="location">
                            <i class="fa fa-cog"></i>
                            Размещение
                    </div>
            </a>
            <a href="#modal-print" data-toggle="modal">
                    <div class="print">
                            <i class="fa fa-delicious"></i>
                            <div class="shir">
                                    Широкоформатная
                                    печать
                            </div>
                     </div>
            </a>
     </div>
      -->
  </div>

  <?php /*
<?php // + Главный блок ?>
    <div id="main">

      <div id="container" class="row">
<?php

$column1 = 0;
$column2 = 9;
$column3 = 0;

if (Yii::app()->menu->current != null) {
  $column1 = 3;
  $column2 = 6;
  $column3 = 3;
  
  if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_LEFT) == 0) {$column1 = 0; $column3 = 4;}
  if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_RIGHT) == 0) {$column3 = 0; $column1 = $column1*4/3;}
  $column2 = 12 - $column1 - $column3;
  if ($column2 == 12) $column2 = 9;
}

?>
        <?php if ($column1 > 0): // левая колонка ?>
        <div id="sidebarLeft" class="col-md-<?php echo $column1; ?>">
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_LEFT)); ?>
        </div>
        <?php endif ?>
        
        <div id="content" class="col-md-<?php echo $column2; ?>">
          <div class="page-header">
            <h1><?php echo $this->caption; ?></h1>
          </div>
          
          <?php if ($this->useBreadcrumbs && isset($this->breadcrumbs)): // Цепочка навигации ?>
          <?php $this->widget('BreadcrumbsWidget', array(
            'homeLink' => array('Главная' => Yii::app()->homeUrl),
            'links' => $this->breadcrumbs,
          )); ?>
          <?php endif ?>

          <div class="cContent">
            <?php echo $content; ?>
          </div>
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_BOTTOM)); ?>
        </div>

        <?php if ($column3 > 0): // левая колонка ?>
        <div id="sidebarRight" class="col-md-<?php echo $column3; ?>">
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_RIGHT)); ?>
        </div>
        <?php endif ?>

      </div>
<?php //Тут возможно какие-нить модули снизу ?>
      <div class="clr"></div>
    </div>
<?php // - Главный блок ?>
*/
    echo $content;
  ?>
  <div id="back-top"><span>↑</span></div>

</div>

<div id="footer" class="container">
  <div class="row">
    <div class="col-xs-3 logo">
      <img alt="Логотип компании" src="/themes/business/gfx/logo_footer.png">
    </div>
    <div class="col-xs-6 info">


      <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_FOOTER)); ?>
    </div>
    <div class="col-xs-2 logo-cvek">
      <div id="cvek">
        <a title="создать сайт в Цифровом веке" href="http://cvek.ru">Создание сайта — веб-студия &laquo;Цифровой
          век&raquo;</a>
      </div>
    </div>
  </div>
</div>

</div>

<div id="modal" class="modal fade">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Рекламные позиции на карте</h4>
      </div>
      <div class="modal-body">
<!--        <script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=3P3LwpfigJVTjF4Yal4o9V8AhiSSGnj3&width=558&height=450"></script>-->
        <script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=ZFRB84-rkv0gAYpo-9RN98yE3RJwr2o7&width=850&height=600"></script>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="modal-about" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">О компании</h4>
      </div>
      <div class="modal-body">
        <p>Наружная реклама по праву считается одним из самых эффективных способов привлечения новых клиентов.</p>

        <p>Компания «ОК – РЕКЛАМА», основанная в 2006 году, специализируется на размещении и изготовлении наружной
          рекламы в Сыктывкаре, как на рекламных АРКАХ, перетяжках, так и на конструкциях стандартного формата 3м х
          6м.</p>

        <p>Наша компания предложит Вам интересные места для размещения рекламы в городе Сыктывкар и Эжвинском
          районе.</p>

        <p>Компания «ОК – РЕКЛАМА» обрела репутацию профессионалов на рынке услуг наружной рекламы. Нам доверили
          масштабные проекты, такие как: Выборы Президента Российской Федерации, выборы депутатов в Государственную Думу
          РФ.</p>

        <p>Наши заказчики – крупные компании, являющиеся лидерами в своей отрасли ведущие торговые сети и магазины.</p>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="modal-klient" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Клиенты</h4>
      </div>
      <div class="modal-body">
        <p>One fine body</p>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="modal-reviews" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Отзывы</h4>
      </div>
      <div class="modal-body">
        <p>One fine body</p>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="modal-kontakt" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Контакты</h4>
      </div>
      <div class="modal-body">
      	<p></p>
        <p>ИП Корчма Олег Викторович</p>
        <p> +7(912) 86-46-272,  346-272 , 8(8212) 31-50-50</p>
        <p>E-mail :   Korchma-reklama@yandex.ru</p>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="modal-razm" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Варианты размещения</h4>
      </div>
      <div class="modal-body">
        <p>Арки над дорогой:</p>
        <ul>
          <li>Эжвинская - 24 х 3 м.</li>
          <li>Сысольское шоссе - 24 х 5 м.</li>
        </ul>
        <p>Щиты:</p>
        <ul>
          <li> Щит № 1 - 10 км + 960 м.,</li>
          <li> Щит № 2 - 11 км + 70 м.,</li>
          <li> Щит № 3 - возле а/заправки "ЛУКОЙЛ" - 13 км + 680 м.,</li>
          <li> Щит № 4 - возле А/заправки "Компания 2000"</li>
          <li> Щит № 5 - возле стеллы "СЛПК" - 15 км + 880 м.,</li>
          <li> Щит № 6 - ул. Пермская а/дороги Троицко-Печорск (район ул. Колхозная)</li>
          <li> Щит № 7 - трасса Сыктывкар-Троицко-Печорск - 3км + 230 м.</li>

        </ul>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="modal-print" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Широкоформатная печать</h4>
      </div>
      <div class="modal-body">
        <p>В отличие от придорожных щитов, приевшихся взгляду обывателя, крупная арка над проезжей частью никогда не
          будет проигнорирована и обязательно привлечёт взгляд каждого проезжающего автомобилиста.</p>

        <p>Компания "ОК-Реклама" предлагает вам арки на трассах с наивысшей плотностью транспортного потока на обоих
          въездах в Сыктывкар: на эжвинской трассе и на Сысольском шоссе.</p>

        <p>Можете быть уверенны, вашу рекламу заметит каждый!</p>

        <p>Широкоформатная печать – процесс нанесения полноцветных качественных изображений и текстовой информации на
          рекламные носители большого формата.</p>

        <p>Широкоформатная печать используется для проведения масштабных рекламных кампаний, цель которых, - максимально
          охватить целевую аудиторию.</p>

      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


</body>
</html>